# O que é #

SNEP is a Soft Free PBX that meet the needs for any size of Business, with features and resources that bring a communication more free, flexible and smart.

Known more about the project: [SNEP](http://www.sneppbx.com/)

# Build in a collaborative way #

SNEP was build with the collaboration of many developers and users and it have a active community.

# A Ecossystem of Opportunities #

Choice and foccus on your business market, that the ecossystem will do the rest for you.

More than a Software Project the SNEP proposes to create a business ecosystem that allows the performance of different profiles of collaborators: developers, integrators, manufacturers and users.

# How to use #

To install and use SNEP you can follow this manual of installation:
http://www.sneppbx.com/download/

To manual installation, from source, use this documentation:
[Install from source](http://wiki.opens.com.br/display/DSC/SNEP+3+Install+guide)

# How to colaborate #

Welcome to colaborate with the Project!

There are differents ways, translating, reporting, fixing and implementing.

See here some details how to do that:
[How to send contributions](http://wiki.opens.com.br/pages/viewpage.action?pageId=15041384)
